use anyhow::Result;
use futures::stream::StreamExt;
use ipfs_embed::{Cid, Config, DefaultParams, Ipfs};
use log::info;

#[async_std::main]
async fn main() -> Result<()> {
    env_logger::init();

    // From test url ipfs://bafybeiemxf5abjwjbikoz4mc3a3dla6ual3jsgpdr4cjr3oz3evfyavhwq/wiki/Vincent_van_Gogh.html
    let cid: Cid = "bafybeiemxf5abjwjbikoz4mc3a3dla6ual3jsgpdr4cjr3oz3evfyavhwq".parse()?;

    let config = Config::default();
    info!("Configuration is {:?}", config);
    let ipfs = Ipfs::<DefaultParams>::new(config).await.unwrap();
    ipfs.listen_on("/ip4/0.0.0.0/tcp/0".parse()?)?
        .next()
        .await
        .unwrap();

    let mut events = ipfs.swarm_events();

    async_std::task::spawn(async move {
        while let Some(event) = events.next().await {
            info!("Got event {:?}", event);
        }
    });

    loop {
        info!("Peers: {:?}", ipfs.peers());

        if !ipfs.contains(&cid)? {
            // Fetch this cid
            info!("Need to fetch {}", cid);
        } else {
            let block = ipfs.get(&cid)?;

            info!("Got block: {:?}", block);
            break;
        }

        std::thread::sleep(std::time::Duration::from_secs(5));
    }
    Ok(())
}
